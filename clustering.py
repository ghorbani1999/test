import numpy as np
import cv2
cap = cv2.VideoCapture(2)
while(cap.isOpened()):
    ret , frame = cap.read()
    frame =cv2.cvtColor(frame ,cv2.COLOR_BGR2RGB)
    vectorized = frame.reshape((-1,3))
    vectorized = np.float32(vectorized)
    criteria = (cv2.TERM_CRITERIA_EPS + cv2.TERM_CRITERIA_MAX_ITER, 10, 1.0)
    K = 2
    attempts=10
    ret,label,center=cv2.kmeans(vectorized,K,None,criteria,attempts,cv2.KMEANS_PP_CENTERS)
    center = np.uint8(center)
    res = center[label.flatten()]
    result_image = res.reshape((frame.shape))
    ret,thresh = cv2.threshold(result_image,60,255,cv2.THRESH_BINARY_INV)
    canny = cv2.Canny(thresh,130,255)
    #contours,hierarchy = cv2.findContours(canny.copy(), 1, cv2.CHAIN_APPROX_NONE)
    #cv2.drawContours(result_image,contours,-1,(150,255,0),10)
    #if len(contours) > 0:      
    #    try :
    #            c = max(contours, key=cv2.contourArea)
    #            M = cv2.moments(c)
    #            cx = int(M['m10']/M['m00'])
    #            cy = int(M['m01']/M['m00'])
    #            cv2.circle(result_image,(cx,cy),20,(0,0,255),5)
    #    except: continue
    lines = cv2.HoughLines(canny, 1, np.pi / 180, 200)
    try:
        for line in lines:
            rho,theta = line[0]
            a = np.cos(theta)
            b = np.sin(theta)
            x0 = a * rho
            y0 = b * rho
            # x1 stores the rounded off value of (r * cos(theta) - 1000 * sin(theta))
            x1 = int(x0 + 1000 * (-b))
            # y1 stores the rounded off value of (r * sin(theta)+ 1000 * cos(theta))
            y1 = int(y0 + 1000 * (a))
            # x2 stores the rounded off value of (r * cos(theta)+ 1000 * sin(theta))
            x2 = int(x0 - 1000 * (-b))
            # y2 stores the rounded off value of (r * sin(theta)- 1000 * cos(theta))
            y2 = int(y0 - 1000 * (a))
            cv2.line(result_image,(x1,y1),(x2,y2),(120,0,255),10)
            cv2.circle(result_image,(x2,y2),50,(200,120,165),5)
    except:continue
    cv2.imshow("clustering" , result_image)
    key = cv2.waitKey(1)
    if key == 27 :
        break
cap.release()
cv2.destroyAllWindows()

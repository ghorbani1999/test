import numpy as np
import cv2
import math
video_capture = cv2.VideoCapture(0)
while(video_capture.isOpened()):
    ret, frame = video_capture.read()
    if ret==True:
        frame = cv2.flip(frame,0)
    else: break
    try :
         hsv = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)
    except:continue
    low_blue = np.array([85,0,0])
    high_blue = np.array([130,255,255])
    mask = cv2.inRange(hsv,low_blue,high_blue)
    res = cv2.bitwise_and(frame,frame,mask=mask)
    blur = cv2.GaussianBlur(mask,(5,5),0)
    ret,thresh = cv2.threshold(blur,60,255,cv2.THRESH_BINARY_INV)
    canny = cv2.Canny(thresh,130,255)
    lines = cv2.HoughLines(canny, 1, np.pi / 180, 200)
    try:
        for line in lines:
            rho,theta = line[0]
            a = np.cos(theta)
            b = np.sin(theta)
            x0 = a * rho
            y0 = b * rho
            # x1 stores the rounded off value of (r * cos(theta) - 1000 * sin(theta))
            x1 = int(x0 + 1000 * (-b))
            # y1 stores the rounded off value of (r * sin(theta)+ 1000 * cos(theta))
            y1 = int(y0 + 1000 * (a))
            # x2 stores the rounded off value of (r * cos(theta)+ 1000 * sin(theta))
            x2 = int(x0 - 1000 * (-b))
            # y2 stores the rounded off value of (r * sin(theta)- 1000 * cos(theta))
            y2 = int(y0 - 1000 * (a))
            cv2.line(res,(x1,y1),(x2,y2),(120,0,255),10)
            cv2.circle(res,(x2,y2),50,(200,120,165),5)
            print(theta * (180/np.pi))
    except:continue
    contours,hierarchy = cv2.findContours(canny, 1, cv2.CHAIN_APPROX_NONE)
    cv2.drawContours(res,contours,-1,(0,255,0),1)
    if len(contours) > 0:      
        try :
                c = max(contours, key=cv2.contourArea)
                M = cv2.moments(c)
                cx = int(M['m10']/M['m00'])
                cy = int(M['m01']/M['m00'])
                cv2.circle(res,(cx,cy),20,(0,0,255),5)
        except: continue
    cv2.imshow('frame',res)
    if cv2.waitKey(10) & 0xFF == ord('q') :
        break
video_capture.release()
cv2.destroyAllWindows()